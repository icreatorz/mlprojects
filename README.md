# Machine Learning Projects

Repository for ML-related (machine learning) projects in i-CreatorZ Club CLHS.

Only certain members have private permission to access this repo, otherwise this repo is read-only. Hence, no patches/changes/PR will be accepted.

## How to Join?

First of all, please make sure you are willing to leave your current interest group after passed our simple challenge which will be mentioned later.

1) Fork this repository and clone it to your PC.
2) Using any programming language, complete the qualification challenge located in `challenge/`.
3) After completing the challenge, add your name, GitLab username, profile URL and the programming language which will be used in projects to the `COLLABORATORS.md` located in the root of current repository, and create a PR to be reviewed.
4) After reviewing, the PR will be merged and ta-da! you're now a part of the AI Team.
